# OpenML dataset: nomao

https://www.openml.org/d/45078

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Nomao collects data about places (name, phone, localization...) from many sources. Deduplication consists in detecting what data refer to the same place. Instances in the dataset compare 2 spots.The dataset has been enriched during the Nomao Challenge: organized along with the ALRA workshop (Active Learning in Real-world Applications): held at the ECML-PKDD 2012 conference.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45078) of an [OpenML dataset](https://www.openml.org/d/45078). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45078/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45078/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45078/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

